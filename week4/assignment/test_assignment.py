import os
import subprocess
import mfw
import unittest


test_text = """Call me Ishmael.  Some years ago--never mind how long precisely--^M
having little or no money in my purse, and nothing particular^M
to interest me on shore, I thought I would sail about a little^M
and see the watery part of the world."""
test_tokens = ['call', 'me', 'ishmael', 'some', 'years', 'ago', 'never', 'mind', 'how', 'long', 'precisely', 'm', 'having', 'little', 'or', 'no', 'money', 'in', 'my', 'purse', 'and', 'nothing', 'particular', 'm', 'to', 'interest', 'me', 'on', 'shore', 'i', 'thought', 'i', 'would', 'sail', 'about', 'a', 'little', 'm', 'and', 'see', 'the', 'watery', 'part', 'of', 'the', 'world']
test_tokens_filtered = ['call', 'ishmael', 'ago', 'mind', 'precisely', 'little', 'money', 'purse', 'particular', 'shore', 'sail', 'little', 'watery', 'world']
test_counts = [('m', 3), ('me', 2), ('the', 2), ('and', 2), ('little', 2), ('i', 2), ('in', 1), ('purse', 1), ('ishmael', 1), ('see', 1)]


class TokenizerTest(unittest.TestCase):

    def test_tokenizer(self):
        self.assertEqual(mfw.tokenize(test_text), test_tokens)


class WordfreqTest(unittest.TestCase):

    def test_wordfreq(self):
        record = len(test_tokens)
        pairs = mfw.most_frequent_words(test_tokens)
        self.assertEqual(len(pairs), 10, 'The output should contain the 10 most frequent words')
        for word, count in pairs:
            self.assertIn(word, test_tokens, 'The word "{}" is not in the input list and should not appear in the output'.format(word))
            self.assertLessEqual(count, record, 'The pairs in the output are not ordered by descending count')
            witnesses = [w for w in test_tokens if w == word]
            self.assertEqual(count, len(witnesses), 'The count {} for word "{}" is not correct, should be {}'.format(count, word, len(witnesses)))


class StopwordsTest(unittest.TestCase):

    def test_stopwords(self):
        self.assertEqual(mfw.filter_stopwords(test_tokens), test_tokens_filtered)
