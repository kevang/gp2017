#!/usr/bin/env python3


import sys


if __name__ == '__main__':
    try:
        _, path = sys.argv
    except ValueError:
        print('USAGE: python3 mfw.py TEXTFILE', file=sys.stderr)
        sys.exit(1)
    with open(path) as f:
        tokens = tokenize(f.read())
    mfw = most_frequent_words(tokens)
    for word, count in mfw:
        print(count, word)
