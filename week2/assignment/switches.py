#!/usr/bin/env python3


from switches_model import AndSwitchesModel
from switches_view import TextSwitchesView
from switches_controller import SwitchesController


SwitchesController(AndSwitchesModel(), TextSwitchesView()).run()
