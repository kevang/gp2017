import switches_model
import switches_view
import unittest


class ModelTest(unittest.TestCase):

    def test_model(self):
        model = switches_model.OrSwitchesModel()
        self.assertFalse(model.is_switch1_on()) 
        self.assertFalse(model.is_switch2_on()) 
        self.assertFalse(model.is_lightbulb_on()) 
        model.set_switch1(True)
        self.assertTrue(model.is_switch1_on()) 
        self.assertFalse(model.is_switch2_on()) 
        self.assertTrue(model.is_lightbulb_on()) 
        model.set_switch2(True)
        self.assertTrue(model.is_switch1_on()) 
        self.assertTrue(model.is_switch2_on()) 
        self.assertTrue(model.is_lightbulb_on()) 
        model.set_switch1(False)
        self.assertFalse(model.is_switch1_on()) 
        self.assertTrue(model.is_switch2_on()) 
        self.assertTrue(model.is_lightbulb_on()) 
        model.set_switch2(False)
        self.assertFalse(model.is_switch1_on()) 
        self.assertFalse(model.is_switch2_on()) 
        self.assertFalse(model.is_lightbulb_on()) 


class ViewTest(unittest.TestCase):

    def test_view(self):
        view = switches_view.GUISwitchesView
        view.show
        view.hide
        view.display
        view.get_action
