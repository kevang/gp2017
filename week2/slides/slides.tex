\documentclass{beamer}

\usetheme{Singapore}

\usepackage{fancyvrb}
\usepackage{relsize}
\usepackage{tikz-uml}
\usepackage[utf8]{inputenc}

\title{Object-oriented Design}
\author[Kilian Evang]{Kilian Evang\\
\quad\\
Gevorderd programmeren\\
Week 2, lecture}
\date{2017-02-14}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

\begin{frame}
 \tableofcontents
\end{frame}

\section[Recap: OOP]{Recap: Object-oriented Programming}

\subsection{Recap: Object-oriented Programming in Python}

\begin{frame}[t,fragile]
 \frametitle{Classes in Python}
 \smaller 
 \begin{Verbatim}[commandchars=\\\{\}]
# person.py
"""This module provides a class for modeling a typical
person."""

class Person:
"""This class models a person with typical attributes like
first name and last name."""

    def __init__(self, first_name, last_name):
        """Initialize a person by giving her a first and
        last name."""
        self.first_name = first_name
        self.last_name = last_name

    def full_name(self):
        """Returns a string composed from first and last
        name."""
        fname = self.first_name + ' ' + self.last_name
        return fname
 \end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Things to Remember}
  \begin{itemize}
   \item \emph{constructors}, \emph{attributes}, \emph{methods}
   \item the \texttt{self} argument
   \item instance vs. local variables
   \item the \texttt{\_\_str\_\_} method
   \item \emph{modules} and the \texttt{import} statement
   \item docstrings
   \item encapsulation
  \end{itemize}
\end{frame}

\section{UML Basics}

\begin{frame}
 \frametitle{Human-Readable vs. Machine-Readable}
 \begin{itemize}
  \item Python is fairly human-readable
  \item However, even Python code is not always easy to understand
  \begin{itemize}
   \item big projects
   \item different programming styles
   \item complex code
   \item types of methods, arguments and attributes are not clear
  \end{itemize}
  \item Let's \emph{draw} our class!
 \end{itemize}
\end{frame}

\subsection{UML Class Diagrams}

\begin{frame}
 \frametitle{Class Diagram}
 \begin{center}
  \begin{tikzpicture}
   \umlclass[x=0,y=0]{Person}{
     first\_name : str\\
     last\_name : str\\
     address : Address}{
     full\_name() : str\\
     move\_to\_address(Address) : None
     }
  \end{tikzpicture}\pause
  \begin{itemize}
   \item box with three parts: name, attributes, methods
   \item includes attribute types, argument types, method return types
   \item does not include code
  \end{itemize}
 \end{center}
\end{frame}

\begin{frame}
 \frametitle{UML (Unified Modeling Language)}
 \begin{itemize}
  \item a ``language'' of diagrams
  \item purpose:
  \begin{itemize}
   \item design complex programs (before writing code)
   \item in a standardized way
   \item explain programs to other developers (after writing code)
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{UML Diagram Types}
 \includegraphics[width=10cm]{umltypes}
 \begin{itemize}
  \item \url{https://en.wikipedia.org/wiki/File:Uml\_diagram.svg}
  \item in this course: only class diagrams
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{UML Class Diagrams}
 \begin{itemize}
  \item show classes with attributes and methods (see above)
  \item also show \emph{associations} between classes, e.g.
  \begin{itemize}
   \item a \texttt{Person} \textbf{lives at} an \texttt{Address}
   \item a \texttt{Student} \textbf{takes} a number of \texttt{Course}s
   \item a \texttt{Teacher} \textbf{teaches} a \texttt{Course}
  \end{itemize}
 \end{itemize}
\end{frame}

\subsection{Associations}

\begin{frame}
 \frametitle{Associations Example}
 \begin{center}
  \tikzumlset{font=\tiny} 
  \begin{tikzpicture}
   \umlclass[x=0,y=0]{Person}{
     first\_name : str\\
     last\_name : str\\
     address : Address}{
     full\_name() : str\\
     move\_to\_address(Address) : None
     }
   \umlclass[x=6,y=0]{Address}{
    street\_name : str\\
    house\_number : int\\
    house\_number\_suffix : int\\
    postcode : str\\
    place : str\\
   }{
   }
   \umlassoc[arg2=lives at]{Person}{Address}
  \end{tikzpicture}
 \end{center}
\end{frame}

\section[MVC]{Model-View-Controller}

\subsection{Model-View-Controller}

\begin{frame}
 \frametitle{The Model-View-Controller Pattern}
 \begin{itemize}
  \item common way to structure interactive applications
  \item application consists of three main components (objects):
  \begin{itemize}
   \item model: stores the state of the application
   \item view: the user interface (shows the state, receives input)
   \item controller: takes the right action at the right time
  \end{itemize}
  \item components only communicate via well-defined methods
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{MVC Schema}
  \begin{tikzpicture}
   \umlclass[x=0,y=2]{Controller}{
     }{
     }
   \umlclass[x=-4,y=-2]{Model}{
     }{
     }
   \umlclass[x=4,y=-2]{View}{
     }{
     }
   \umlassoc[arg2=consults; updates]{Controller}{Model}
   \umlassoc[arg2=updates; gets user actions]{Controller}{View}
   \umlassoc[arg2=consults]{View}{Model}
  \end{tikzpicture}
\end{frame}

\begin{frame}
 \frametitle{MVC Application Implementing a Very Simple Board Game}
  \tikzumlset{font=\tiny} 
  \begin{tikzpicture}
   \umlclass[x=0,y=2]{GameController}{
     }{
       run() : None
     }
   \umlclass[x=-4,y=-2]{GameModel}{
       position : int
     }{
       getPawnPosition() : int \\
       movePawnBy(fields : int) : None\\
       isLost() : bool\\
       isWon() : bool
     }
   \umlclass[x=3,y=-2]{GameView}{
     }{
       showBoard(model : Model) : None\\
       showMessage(message : str) : None\\
       getUserAction() : Action
     }
   \umlassoc[arg2=consults; updates]{GameController}{GameModel}
   \umlassoc[arg2=updates; gets user actions]{GameController}{GameView}
   \umlassoc[arg2=consults]{GameView}{GameModel}
  \end{tikzpicture}
\end{frame}

%\begin{frame}
% \frametitle{The Model-View-Controller Architecture}
% \begin{itemize}
%  \item a common way to design interactive applications
%  \item an example of \emph{separation of concerns}
%  \item separate the application into three components:
%  \begin{itemize}
%   \item model: stores application state
%   \item view: the user interface
%   \item controller: controls what happens, and when
%  \end{itemize}
%  \item example: a board game
% \end{itemize}
%\end{frame}
%
%\begin{frame}
% \frametitle{Implementing MVC in an Object-oriented Way}
% \begin{itemize}
%  \item application creates three main objects of different classes: model, view, controller
%  \item they communicate only via well-defined interface (methods designed to be called from outside the object), do not access each other's attributes directly (\emph{encapsulation})
% \end{itemize}
%\end{frame}
%
\begin{frame}
 \frametitle{Some Benefits of Separating Concerns}
 \begin{itemize}
  \item small, specialized components lead to better overview
  \item less risk of programmer getting confused by unrelated code while working on one concern
  \item less risk of programmer inadvertently breaking unrelated code
  \item easier for multiple programmers to work simultaneously on project
  \item components have interfaces/contracts $\rightarrow$ less risk of code growing chaotic
  \item components can be tested independently of each other
  \item components can be substituted, e.g. the same controller/model can be used with either a text-based view or a GUI view
 \end{itemize}
\end{frame}
%
%\begin{frame}
% \frametitle{Tasks of the Model}
% \begin{itemize}
%  \item store the state of the application
%  \begin{itemize}
%   \item positions of pawns on the board
%   \item how much money each player has
%   \item ...
%  \end{itemize}
%  \item define valid changes to the state
%  \begin{itemize}
%   \item move pawn in a way allowed by rules
%   \item 
%  \end{itemize}
% \end{itemize}
%\end{frame}
%
\section{Class Hierarchies}

\subsection{Class Hierarchies}

\begin{frame}
 \frametitle{Class Hierarchies}
 \begin{block}{Recap}
  \begin{itemize}
   \item objects model real-world things
   \item classes model \emph{kinds} of things
  \end{itemize}
 \end{block}
 \begin{block}{A new important concept in OOP}
  \begin{itemize}
   \item class hierarchies
   \item based on observations: kinds of things are in subclass-superclass
         relationships
  \end{itemize}
 \end{block}
\end{frame}

\begin{frame}
 \frametitle{Examples of Subclass-Superclass Relationships}
 \begin{itemize}
  \item a \textbf{student} is a \textbf{person}
  \item a \textbf{professor} is a \textbf{person}
  \item an \textbf{apple} is a \textbf{fruit}
  \item a \textbf{banana} is a \textbf{fruit}
  \item a \textbf{vertebrate} is an \textbf{animal}
  \item a \textbf{mammal} is a \textbf{vertebrate}
  \item a \textbf{zebra} is a \textbf{mammal}
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{An Example Class Hierarchy}
 \begin{center}
  \begin{tikzpicture}[level 1/.style={sibling distance=20mm}]
   \node {Organism}
    child { node {Plant} }
    child { node {Fungus} }
    child { node {Animal}
     child { node {Mollusc} }
     child { node {Vertebrate}
      child { node {Fish} }
      child { node {Amphibian} }
      child { node {Reptile} }
      child { node {Bird} }
      child { node {Mammal} }}};
  \end{tikzpicture}
 \end{center}
\end{frame}

\begin{frame}
 \frametitle{Another Example Class Hierarchy}
 \begin{center}
  \begin{tikzpicture}[level 1/.style={sibling distance=20mm}]
   \node {Person}
    child { node {Student} }
    child { node {Professor} };
  \end{tikzpicture}
 \end{center}\pause
 Some terminology:
 \begin{itemize}
  \item \texttt{Student} and \texttt{Professor} are \textbf{subclasses} of
        \texttt{Person}
  \item \texttt{Person} is the \textbf{superclass} of \texttt{Student} and
        \texttt{Professor}
 \end{itemize}
\end{frame}

\subsection{Inheritance}

\begin{frame}
 \frametitle{How Do Class Hierarchies Help Us with Programming?}
 \begin{itemize}
  \item Classes in a hierarchy have many attributes and methods in common.
  \item E.g., students and professors, because they are persons,
  \begin{itemize}
   \item have a \texttt{first\_name} and a \texttt{last\_name}
   \item support a \texttt{full\_name} method
  \end{itemize}
  \item No need to repeat these attributes and methods in each class
        thanks to \emph{inheritance}.
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Inheritance}
 \begin{itemize}
  \item a subclass \emph{inherits} all attributes and methods from its
        superclass
  \item no need to repeat them in subclass, either in UML or Python
%  \item in Python, inheriting attributes is a side-effect of inheriting
%        instance variables, which is a side-effect of inheriting methods
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{A Class Hierarchy in UML}
 \smaller\smaller\smaller
 \begin{center}
  \tikzumlset{font=\tiny} 
  \begin{tikzpicture}
   \umlclass[x=0,y=0]{Person}{
     first\_name : str\\
     last\_name : str\\
     address : Address}{
     full\_name() : str\\
     move\_to\_address(Address) : None
     }
   \umlemptyclass[x=-2,y=-3]{Student}
   \umlclass[x=2,y=-3]{Professor}{
     department : str
    }{
     get\_department() : str\\
     set\_department(str) : None
     }
   \umlinherit{Student}{Person}
   \umlinherit{Professor}{Person}
  \end{tikzpicture}
 \end{center}
\end{frame}

\begin{frame}
 \frametitle{A UML Class Diagram with Association and Inheritance}
 \begin{center}
  \tikzumlset{font=\tiny} 
  \begin{tikzpicture}
   \umlclass[x=-2,y=0]{Person}{
     first\_name : str\\
     last\_name : str\\
     address : Address}{
     full\_name() : str\\
     move\_to\_address(Address) : None
     }
   \umlclass[x=4,y=0]{Address}{
    street\_name : str\\
    house\_number : int\\
    house\_number\_suffix : int\\
    postcode : str\\
    place : str\\
   }{
   }
   \umlemptyclass[x=-2,y=-3]{Student}
   \umlclass[x=2,y=-3]{Professor}{
     department : str
    }{
     get\_department() : str\\
     set\_department(str) : None
     }
   \umlassoc[arg2=lives at]{Person}{Address}
   \umlinherit{Student}{Person}
   \umlinherit{Professor}{Person}
  \end{tikzpicture}
  \begin{itemize}
   \item association relation: labeled lines
   \item inheritance relation: arrows
  \end{itemize}
 \end{center}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{Defining a Subclass in Python}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):
    pass
 \end{Verbatim}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{Specify the Superclass}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor\textcolor{red}{(Person)}:
    pass
 \end{Verbatim}
 \begin{itemize}
  \item in parentheses after the subclass name
  \item this automatically copies all methods from the superclass
 \end{itemize}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{Aside: Empty Blocks}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):
    \textcolor{red}{pass}
 \end{Verbatim}
 \begin{itemize}
  \item in this example, the class body is empty
  \item Python then requires \texttt{pass} as a placeholder
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Inheritance in Action}
 \begin{Verbatim}[commandchars=\\\{\}]
>>> p = Professor('Jane', 'Smith')
>>> p.full_name()
'Jane Smith'
 \end{Verbatim}
 \begin{itemize}
  \item It works!
  \item all methods inherited from \texttt{Person}
 \end{itemize}
\end{frame}

\subsection{Specialization}

\begin{frame}[fragile]
 \frametitle{Specialization}
 The point of having a subclass is to \emph{specialize} it with attributes and
 methods that are not applicable to the superclass.\pause
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):

    def set_department(self, department):
        self.department = department

    def get_department(self):
        return self.department

    def teach(self, course):
        print('Welcome to the course \{0\}!'.format(course))
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Specialization in Action}
 \begin{Verbatim}[commandchars=\\\{\}]
>>> p = Professor('Minerva', 'MgGonagall')
>>> p.set_department('Transfiguration')
>>> p.get_department()
'Transfiguration'
>>> p.teach('Advanced Transfiguration')
Welcome to the course Advanced Transfiguration!
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Inheritance is a One-Way Street}
 \begin{Verbatim}[commandchars=\\\{\}]
>>> q = Person('Severus', 'Snape') # not specific enough!
>>> q.set_department('Potions')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'Person' object has no attribute
'set_department'
 \end{Verbatim}
 \begin{itemize}
  \item superclass \texttt{Person} does not inherit from subclass
 \end{itemize}
\end{frame}

\subsection{Overriding}

\begin{frame}[fragile]
 \frametitle{Greeting}
 Let's add a \texttt{greet} method to our \texttt{Person} class:
 \begin{Verbatim}[commandchars=\\\{\}]
class Person:

    # ...

    def greet(self):
        print('Hi, I'm \{0\}.'.format(self.full_name()))
 \end{Verbatim} 
\end{frame}

\begin{frame}
 \frametitle{Overriding}
 \begin{itemize}
  \item \texttt{Student} and \texttt{Professor} inherit \texttt{greet}
  \item makes sense, they can surely greet
  \item however, they might do it differently:
  \begin{itemize}
   \item less formal greeting for students: \texttt{Hi, I'm Harry!}
   \item more formal greeting for professors: \texttt{Good morning, I'm
         Professor McGonagall}
  \end{itemize}
  \item no problem: subclasses can \emph{override} methods
 \end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Overriding}
 \begin{itemize}
  \item subclasses can \emph{override} inherited methods by redefining them with
  \begin{itemize}
   \item the same name
   \item the same number and type of arguments
   \item the same return type
   \item the same rough purpose (\emph{what} to do)
   \item an individual implementation (\emph{how} to do it)
  \end{itemize}
  \item e.g. \texttt{apple.peel()} and \texttt{banana.peel()} are probably
        implemented differently
  \item calling code need not know about differences
 \end{itemize}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Overriding}
 \begin{Verbatim}[commandchars=\\\{\}]
class Student(Person):

    def greet(self):
        print('Hi, I'm \{0\}.'.format(self.first_name))

class Professor(Person):

    # ...

    def greet(self):
        print('Good morning, I'm Professor \{0\}.'.format(
                self.last_name))
 \end{Verbatim}
\end{frame}

\begin{frame}[fragile]
 \frametitle{Overriding in Action}
 \begin{Verbatim}[commandchars=\\\{\}]
>>> people = []
>>> people.append(Person('Argus', 'Filch'))
>>> people.append(Student('Luna', 'Lovegood'))
>>> people.append(Professor('Severus', 'Snape'))
>>> for p in people:
...     p.greet()
...
Hi, I'm Argus Filch.
Hi, I'm Luna.
Good morning, I'm Professor Snape.
 \end{Verbatim}
\end{frame}
 
\begin{frame}[fragile]
 \frametitle{Potential Trouble with New Attributes}
 \begin{Verbatim}[commandchars=\\\{\}]
>>> p = Professor('Minerva', 'McGonagall')
>>> p.get_department()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "university.py", line 7, in get_department
AttributeError: 'Professor' object has no attribute
'department'
 \end{Verbatim}
 \begin{itemize}
  \item \texttt{department} instance variable hasn't been created yet
  \item we can guarantee its creation for \texttt{Professor}s by overriding the
        constructor
 \end{itemize}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{Overriding the Constructor}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):

    def __init__(self, first_name, last_name, department):
         Person.__init__(self, first_name, last_name)
         self.department = department
 \end{Verbatim}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{More Arguments}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):

    def __init__(\textcolor{red}{self, first_name, last_name, department}):
         Person.__init__(self, first_name, last_name)
         self.department = department

    # ...
 \end{Verbatim}
 \begin{itemize}
  \item constructor: common exception to the ``same number of arguments'' rule
        in overriding
 \end{itemize}
\end{frame}

\begin{frame}[t,fragile]
 \frametitle{Calling the Superclass Constructor}
 \begin{Verbatim}[commandchars=\\\{\}]
class Professor(Person):

    def __init__(self, first_name, last_name, department):
         \textcolor{red}{Person.__init__(self, first_name, last_name)}
         self.department = department
 \end{Verbatim}
 \begin{itemize}
  \item no difference in initializing first and last name $\rightarrow$
        just call the existing \texttt{Person} constructor (DRY)
  \item also works with ordinary methods, sometimes useful
  \item to call a superclass method, you have to use the form
        \texttt{Superclass.method(self, ...)} rather than
        \texttt{object.method(...)}
 \end{itemize}
\end{frame}

\section{References}
 
\subsection{References}

\begin{frame}
 \frametitle{References}
 \begin{block}{UML}
  \begin{itemize}
   \item \url{http://www.uml.org}
   \item Tools for drawing UML diagrams:
   \begin{itemize}
    \item DIA: \url{https://live.gnome.org/Dia}
    \item TikZ-UML (for \LaTeX): \url{http://www.ensta-paristech.fr/~kielbasi/tikzuml/index.php?lang=en}
    \item paper and pencil
   \end{itemize}
  \end{itemize}
 \end{block}
 \begin{block}{Reading assignment}
  \begin{itemize}
   \item Understand concepts on ``Things to Remember'' slide, look them up in Zelle's book if needed!
   \item John Zelle, \emph{Python Programming}, ch. 12
  \end{itemize}
 \end{block}
 Some slides originally by Valerio Basile
\end{frame}

\end{document}
